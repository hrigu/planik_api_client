
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "planik_api_client/version"

Gem::Specification.new do |spec|
  spec.name          = "planik_api_client"
  spec.version       = PlanikApiClient::VERSION
  spec.authors       = ["hrigu"]
  spec.email         = ["christian.muehlethaler@optor.ch"]

  spec.summary       = %q{Client für die Planik API}
  spec.description   = %q{Client für die Planik API}
  spec.homepage      = "https://planik.ch"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # For authentification (Login)
  # https://github.com/rest-client/rest-client
  spec.add_runtime_dependency 'rest-client', '>= 2.0.2'

  # https://github.com/JsonApiClient/json_api_client
  spec.add_runtime_dependency 'json_api_client', '>= 1.5.3'

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency 'rspec'
end
