module PlanikApiClient
  class Environment

    attr_reader :name
    attr_reader :domain_name, :port
    attr_reader :api_home
    attr_reader :verify_ssl

    def initialize name, domain_name, port, api_home, verify_ssl = true
      @name = name
      @domain_name = domain_name
      @port = port
      @api_home = api_home
      @verify_ssl = verify_ssl

    end

    def url
      #domain_name
      _port = port == 443 ? "": ":#{port}"
      "https://#{domain_name}#{_port}"
    end

  end

end

