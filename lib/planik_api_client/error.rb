
module PlanikApiClient
  class Base < StandardError; end
  class Unauthorized < Base; end
  class InternalServerError < Base; end
  class NotFound < Base; end

end

