require 'json_api_client'
require 'rest_client'

module PlanikApiClient

  class PlanikResource < JsonApiClient::Resource

    self.site = "#{PlanikApiClient::Configuration.environment.url}/#{PlanikApiClient::Configuration.environment.api_home}/" #https://0.0.0.0:3000/api/v1/"
    self.connection_class = PlanikApiClient::Connection

    # Auf Integration und Development Kein korrektes SSL
    connection_options[:ssl] = {verify: false}

    self.connection do |conn|
      conn.use Faraday::Response::Logger
    end
  end

  class Dienst < PlanikResource

  end

  class Mitarbeiter < PlanikResource
  end

  class Anstellung < PlanikResource
  end

  class Arbeitsvertrag < PlanikResource
  end

  class Login < PlanikResource


    # Hier sind die Headers für die folgenden API Abrfagen abgespeichert
    # Ist eine Hasn mit uid, access_token, und client
    #
    attr_reader :access_infos

    #
    # Das Login.
    # Loggt sich mit username und Passwort in Planik ein und speichert die Access Informationen in :access_infos
    #
    def save

      begin

        response = RestClient::Request.execute(
            method: :post,
            url: "#{PlanikResource.site}#{Login.resource_path}",
            headers: nil,
            payload: attributes,
            verify_ssl: PlanikApiClient::Configuration.environment.verify_ssl
        )

        @access_infos = {
            uid: response.headers[:uid],
            access_token: response.headers[:access_token],
            client: response.headers[:client]
        }

        true

      rescue RestClient::Unauthorized => e
        message = "Die Anfrage konnte nicht authorisiert werden"
        errors.add(:base, :not_authorized, message: message)
        # raise PlanikApiClient::Unauthorized, message
        false

      rescue RestClient::InternalServerError => e
        message = "Fehler auf der Serverseite: #{e.message}"
        # raise PlanikApiClient::InternalServerError, message
        errors.add(:base, :internal_server_error, message: message)
        false

      rescue RestClient::NotFound => e
        message = "Die Daten wurden auf der Serverseite nicht gefunden"
        errors.add(:base, :not_found, message: message)
        # raise PlanikApiClient::NotFound, message
        false
      end


    end
  end

end