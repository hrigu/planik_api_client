module PlanikApiClient

  #
  # Der Fetcher holt die Infos vom Server und kümmert sich um das Login
  #
  class Fetcher

    #
    # @param username
    # @param password
    #
    def initialize(username, password)
      @username, @password = username, password
      @logged_in = false
    end

    #
    # Holt die Ressourcen von Planik.
    # Loggt sich zuerst ein falls nötig
    # @param resource_class: Die Ressourceklasse, wie PlanikApiClient::Dienst
    # @param filter: Ein Hash mit allen Filterinfos
    # @param included: Die inkludierten Ressourcen
    #
    def fetch resource_class, filter, included = []
      logged_in? || login!
      begin
        result = nil
        resource_class.with_headers(@login.access_infos) do
          result = resource_class.where(filter).includes(included).all
        end
        result
      rescue JsonApiClient::Errors::ServerError => e
        puts e.env.body
        raise e
      end
    end

    private

    def logged_in?
      @logged_id
    end


    def login!
      @login = PlanikApiClient::Login.create(benutzer: {email: @username, password: @password})
      if @login.errors.empty?
        @logged_in = true
        puts "Erfolgreich in Planik eingeloggt"
      else
        puts @login.errors.full_messages
        raise "Login gescheitert für Benutzer '#{@username}'"
      end
    end


  end
end