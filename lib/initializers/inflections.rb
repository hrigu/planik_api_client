require 'active_support/inflections'

ActiveSupport::Inflector.inflections(:en) do |inflect|
#ActiveSupport::Inflector.inflections do |inflect|

  resources = {
      "dienst" => "dienste",
      "anstellung" => "anstellungen",
      "arbeitsvertrag" => "arbeitsvertraege",
      "mitarbeiter" => "mitarbeiter",
      "login" => "login",
  }

  resources.each_pair do |k, v|
    inflect.irregular k, v
  end

end