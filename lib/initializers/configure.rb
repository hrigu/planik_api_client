require 'yaml'


# Aus der configuration
my_config = YAML.load_file('../config.yml')

#
# PlanikApiClient konfigurieren
#
PlanikApiClient::Configuration.configure do |config|
  config.email = my_config['email']
  config.password = my_config['password']

  env_name = my_config['used_environment']
  env = my_config['environments'][env_name]
  
  environment = PlanikApiClient::Environment.new(
      env['name'],
      env['domain_name'],
      env['port'],
      env['api_home'],
      env['verify_ssl']
  )
  config.environment = environment
  config
end

