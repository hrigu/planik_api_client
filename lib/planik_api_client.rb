require "planik_api_client/configuration"
require "planik_api_client/environment"
require "initializers/configure"
require "initializers/inflections"
require "planik_api_client/version"
require "planik_api_client/error"
require "planik_api_client/connection"
require "planik_api_client/resources"
require "planik_api_client/fetcher"

module PlanikApiClient
  
end
