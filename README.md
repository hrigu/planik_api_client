# PlanikApiClient
Client um die JSON-API von Planik abzufragen.

## Endpoints
Siehe dazu auch https://www.planik.ch/app/api/v1/docs/
- dienste
- mitarbeiter. 

Aktuell gibt es zwei Beispielfiles:
- sandbox/fetch_dienste.rb
- sandbox/fetch_mitarbeiter.rb

   

## Installation
- Das config_template.yml zu config.yml kopieren und anpassen
- Die benötigten gems installieren: $ bundle install                                        


## Usage



TODO: Write usage instructions here
