require 'pp'
require_relative 'sandbox_context'


fetcher = PlanikApiClient::Fetcher.new(PlanikApiClient::Configuration.email, PlanikApiClient::Configuration.password)

# datum_von = Date.today
# datum_bis = datum_von + 30.days
datum_von = Date.parse("2018-07-19")
datum_bis = datum_von + 1.day

filter = {

    # zwingender Filter
    mandant_id: 509,

    # optional. Default: Ab heute 30 Tage in die Zukunft
    zwischen: [datum_von, datum_bis],

    # optional. Default: true.
    # nur_zugewiesene: false,

    # Optional. Default: Alle Dienste.

    # Bsp: Nur Dienste vom Typ "Dienst" (Auch "Ferien", "Abwesenheit" möglich). Kumulierbar
    dienst_typ: ["Dienst"],


    # Optional. 'OR'-Verknüpfung
    # schlagwort: ["hoho"],

    #
    # Jetzt die Filter auf dem Mitarbeiter
    #

    # Nur Dienste der Mitarbeiter mit den gegebenen Personalnummern
    # mitarbeiter_personalnummer: [1004],
    # mitarbeiter_schlagwort: ['gl'],
    # mitarbeiter_kuerzel: ['BB'],

    # Hat nur Einfluss auf die Anstellungen
    # Damit nur die Anstellungen kommen, welche auch in der Abfrageperiode relevant sind
    # anstellungen: {
    #     aktiv_zwischen: [datum_von, datum_bis]
    #
    # }

}

#
# Alle inkludierte Ressourcen
#
included = [
    :mitarbeiter, 'mitarbeiter.anstellungen', 'mitarbeiter.anstellungen.arbeitsvertrag'
]

#
# Hole die Dienste
#
dienste = fetcher.fetch(PlanikApiClient::Dienst, filter, included)


puts "Anzahl Dienste: #{dienste.length}"

pp dienste.map {|d|
  #puts JSON.pretty_generate(d.as_json)
  {
      kuerzel: d.kuerzel, datum: d.datum_zeit_von,
      ma: {
          kuerzel: d.mitarbeiter.kuerzel,
          personalnummer: d.mitarbeiter.personalnummer,
          schlagworte: d.mitarbeiter.schlagworte
      },
      schlagworte: d.schlagworte,
      dienst_typ: d.dienst_typ
  }
}


# dienst = x.first
# puts "erster Dienst: #{dienst.kuerzel}"
# mitarbeiter = dienst.mitarbeiter
# puts "mitarbeiter: #{mitarbeiter.vorname}"
#
# puts JSON.pretty_generate(dienst.as_json)
#
#
# puts JSON.pretty_generate(mitarbeiter.as_json)
#
# puts "Anstellungen: #{mitarbeiter.anstellungen.length}"
# first_anstellung = mitarbeiter.anstellungen.first
# puts JSON.pretty_generate(first_anstellung.as_json)
#
# puts "Arbeitsvertrag"
# puts JSON.pretty_generate(first_anstellung.arbeitsvertrag.as_json)
#
# # dienst.mitarbeiter.anstellungen.each do |a|
# #   puts a.attributes
# # end



