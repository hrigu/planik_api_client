require_relative 'sandbox_context'
require 'pp'


fetcher = PlanikApiClient::Fetcher.new(PlanikApiClient::Configuration.email, PlanikApiClient::Configuration.password)


datum_von = Date.today
datum_bis = datum_von + 1.day

filter = {
    mandant_id: 509,

    # Optional. Wenn nicht gesetzt, dann alle Mitarbeiter, auch die schon ausgetretenen
    # Alle Mitarbeiter, welche mindestens an einem Tag in der Zeitperiode gearbeitet haben
    #angestellt_zwischen: {datum_von: datum_von, datum_bis: datum_bis},

    # Nur die Mitarbeiter mit diesen IDs
    #id: [23542, 234],
    #personalnummer: [24545]
    #kuerzel: ["ansu"],
    #
    # Nur Mitarbeiter mit einer dieser Schlagworte
    #schlagwort: ["gl"],

    # Damit nur die Anstellungen kommen, welche auch in der Abfrageperiode relevant sind
    anstellungen: {
        aktiv_zwischen: {
            datum_von: datum_von,
            datum_bis: datum_bis
        }

    }
}

included = [
    'anstellungen' #, 'anstellungen.arbeitsvertrag'
]


mitarbeiter = fetcher.fetch(PlanikApiClient::Mitarbeiter, filter, included)


puts "Alle Mitarbeiter: #{mitarbeiter.length}"
pp mitarbeiter.as_json

puts "\nErster Mitarbeiter:"
erster_mitarbeiter = mitarbeiter.first
pp erster_mitarbeiter.as_json
#
puts "\nErste Anstellung:"
anstellung = erster_mitarbeiter.anstellungen.first
pp anstellung.as_json


#
# arbeitsvertrag = anstellung.arbeitsvertrag
#
# pp arbeitsvertrag.as_json
# # x.each do |m|
# #   pp m.anstellungen.as_json
# # end


# dienst.mitarbeiter.anstellungen.each do |a|
#   puts a.attributes
# end


